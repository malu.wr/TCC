<?php

if( $_FILES ) { // Verificando se existe o envio de arquivos.
	
	if( $_FILES['model'] ) { // Verifica se o campo não está vazio.
		
		$dir = '../models/'; // Diretório que vai receber o arquivo.
		$tmpName = $_FILES['model']['tmp_name']; // Recebe o arquivo temporário.
		$name = $_FILES['model']['name']; // Recebe o nome do arquivo.
		
		// move_uploaded_file( $arqTemporário, $nomeDoArquivo )
		if( move_uploaded_file( $tmpName, $dir . $name ) ) { // move_uploaded_file irá realizar o envio do arquivo.		
			// sucesso:
			// implementar depois: pegar o valor de $dir . $name, junto com os POSTS de título e etc e colocar no banco
			
			header('Location: ../modelview.php'); // Em caso de sucesso, retorna para a página de sucesso.			
		} else {			
			header('Location: uploaderror.php'); // Em caso de erro, retorna para a página de erro.			
		}
		
	}

}

?>