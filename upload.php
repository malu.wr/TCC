<?php
// Start the session
session_start();
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>3DeeP</title>
		<script type="text/javascript" src="jsc3d/jsc3d.js"></script>
        <script type="text/javascript" src="jsc3d/jsc3d.webgl.js"></script>
        <script type="text/javascript" src="jsc3d/jsc3d.touch.js"></script>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="Description" lang="en" content="ADD SITE DESCRIPTION">
		<meta name="author" content="ADD AUTHOR INFORMATION">
		<meta name="robots" content="index, follow">

		<!-- icons -->
		<link rel="apple-touch-icon" href="assets/img/apple-touch-icon.png">
		<link rel="shortcut icon" href="favicon.ico">

		<!-- Bootstrap Core CSS file -->
		<link rel="stylesheet" href="assets/css/bootstrap.min.css">

		<!-- Override CSS file - add your own CSS rules -->
		<link rel="stylesheet" href="assets/css/styles.css">

		<!-- Conditional comment containing JS files for IE6 - 8 -->
		<!--[if lt IE 9]>
			<script src="assets/js/html5.js"></script>
			<script src="assets/js/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>

		<!-- Navigation -->
	    <nav class="navbar navbar-fixed-top navbar-inverse" role="navigation">
			<div class="container-fluid">

				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="index.php">3DeeP</a>
				</div>
				<!-- /.navbar-header -->

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
			<?php
					//Verifica se usuário está logado para definir o que vai aparecer no botão:
					//A opção "Login" aparecerá para o usuário não logado
					//A opção "Logout aparecerá para um usuário que não esteja logado"
					
					if( isset( $_SESSION['login'] ) ) {
                    $menu_loginlogout ='<a href="php/logout.php">Logout</a>';
					$menu_usernameregister = '<a href="userpage.php">'.$_SESSION['logged_user_name'].'</a>';
					$menu_uploadlogin = '<a href="upload.php">Upload Model</a>';
                    }else {
                    $menu_loginlogout ='<a href="login.php">Login</a>';
					$menu_usernameregister ='<a href="register.php">Register</a>';
					$menu_uploadlogin = '<a href="login.php">Upload Model</a>';
                    }
					?>
					    <li><?php echo $menu_loginlogout; ?></li>												
						<li><?php echo $menu_uploadlogin; ?></li>					
						<li><?php echo $menu_usernameregister; ?></li>
					</ul>
				</div>
				<!-- /.navbar-collapse -->
			</div>
			<!-- /.container-fluid -->
		</nav>
		<!-- /.navbar -->

		<!-- Page Content -->
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-12">
					<div class="page-header">
						<h1>Model Upload</h1>
						
					</div>
				</div>
			</div>
			<!-- /.row -->




			
	

<form enctype="multipart/form-data" action="php/file.php" method="POST">	
<tr>
<td>
<!-- model upload -->
<input type="hidden" name="MAX_FILE_SIZE" value="30000" />

 <b>.OBJ:</b> 
 <input type="file" name="model" accept=".obj">
 
</td>
<td><b>Texture:</b> 
<input type="file" name="texture" accept="image/*"><br></td>

<td><b>Title:</b></td>
<td><input type="text" name="title" value=""><br></td>

<td><b>Description:</b><br></td>
<td><textarea name="modeldescription" value="" rows="4" cols="50"></textarea></td><br>

<td><b>Tags:</b></td>
<td><input type="text" name="modeltags" value=""><br><em>Separate each tag with a space. E.g: "rose model 3d teacup pastel".</em></td>
<br><br>
<td><b>Preview Picture:</b> 
<input type="file" name="preview" accept="image/*"><br></td>

</tr>  
  <input type="submit" value="Submit">     
</form>




			<hr>
			<footer class="margin-tb-3">
				<div class="row">
					<div class="col-lg-12">
						<p>Copyright &copy; 3DeeP 2016. <a href="termsofuse.php">Terms of Use</a></p>
					</div>
				</div>
			</footer>
		</div>
		<!-- /.container-fluid -->

		<!-- JQuery scripts -->
	    <script src="assets/js/jquery-1.11.2.min.js"></script>

		<!-- Bootstrap Core scripts -->
		<script src="assets/js/bootstrap.min.js"></script>
  </body>
</html>

