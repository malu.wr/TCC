<?php
// Start the session
session_start();
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>3DeeP</title>
		<script type="text/javascript" src="jsc3d/jsc3d.js"></script>
        <script type="text/javascript" src="jsc3d/jsc3d.webgl.js"></script>
        <script type="text/javascript" src="jsc3d/jsc3d.touch.js"></script>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="Description" lang="en" content="ADD SITE DESCRIPTION">
		<meta name="author" content="ADD AUTHOR INFORMATION">
		<meta name="robots" content="index, follow">

		<!-- icons -->
		<link rel="apple-touch-icon" href="assets/img/apple-touch-icon.png">
		<link rel="shortcut icon" href="favicon.ico">

		<!-- Bootstrap Core CSS file -->
		<link rel="stylesheet" href="assets/css/bootstrap.min.css">

		<!-- Override CSS file - add your own CSS rules -->
		<link rel="stylesheet" href="assets/css/styles.css">

		<!-- Conditional comment containing JS files for IE6 - 8 -->
		<!--[if lt IE 9]>
			<script src="assets/js/html5.js"></script>
			<script src="assets/js/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>

		<!-- Navigation -->
	    <nav class="navbar navbar-fixed-top navbar-inverse" role="navigation">
			<div class="container-fluid">

				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="index.php">3DeeP</a>
				</div>
				<!-- /.navbar-header -->

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
				<?php
					//Verifica se usuário está logado para definir o que vai aparecer no botão:
					//A opção "Login" aparecerá para o usuário não logado
					//A opção "Logout aparecerá para um usuário que não esteja logado"
					
					if( isset( $_SESSION['login'] ) ) {
                    $menu_loginlogout ='<a href="php/logout.php">Logout</a>';
					$menu_usernameregister = '<a href="userpage.php">'.$_SESSION['logged_user_name'].'</a>';
					$menu_uploadlogin = '<a href="upload.php">Upload Model</a>';
                    }else {
                    $menu_loginlogout ='<a href="login.php">Login</a>';
					$menu_usernameregister ='<a href="register.php">Register</a>';
					$menu_uploadlogin = '<a href="login.php">Upload Model</a>';
                    }
					?>
					    <li><?php echo $menu_loginlogout; ?></li>												
						<li><?php echo $menu_uploadlogin; ?></li>					
						<li><?php echo $menu_usernameregister; ?></li>
					</ul>
				</div>
				<!-- /.navbar-collapse -->
			</div>
			<!-- /.container-fluid -->
		</nav>
		<!-- /.navbar -->

		<!-- Page Content -->
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-12">
					<div class="page-header">
						<h1>[TITLE]</h1> by [username]
						<hr>
						<p>[description]</p>
						<em><p>[tags]</p></em>
					</div>
				</div>
			</div>
			<!-- /.row -->

<div style="width:800px; margin:auto; position:relative;">
    <canvas id="cv" style="border: 1px solid;" width="750" height="400">
        It seems you are using an outdated browser that does not support canvas :-(
    </canvas>
			<br>Drag mouse to rotate <br> Drag mouse with shift pressed or use mouse wheel to zoom<br> Drag mouse with control pressed to move
	<!-- RENDER TYPE SELECT 
	
	<div style="float:right;">
		<select id="render_mode_list">
		<option>Points</option>
		<option>Wireframe</option>
		<option>Flat</option>
		<option>Smooth</option>
		<option>Environment</option>
		</select>
		<button id="change" onclick="setRenderMode();">Change</button>
		</div>
		-->
</div>


<script type="text/javascript">
        var viewer = new JSC3D.Viewer(document.getElementById('cv'));
        viewer.setParameter('SceneUrl', 'models/chaleira.obj' );
        viewer.setParameter('ModelColor',       '#000000');
        viewer.setParameter('BackgroundColor1', '#808080');
        viewer.setParameter('BackgroundColor2', '#ffffff');
        viewer.setParameter('RenderMode',       'flat');
        viewer.init();
        viewer.update();

		
		function setRenderMode() {
			if(logoTimerID > 0)
				return;
			var modes = document.getElementById('render_mode_list');
			switch(modes.selectedIndex) {
			case 0:
				viewer.setRenderMode('point');
				JSC3D.console.logInfo('Set to point mode.');
				break;
			case 1:
				viewer.setRenderMode('wireframe');
				JSC3D.console.logInfo('Set to wireframe mode.');
				break;
			case 2:
				viewer.setRenderMode('flat');
				JSC3D.console.logInfo('Set to flat mode.');
				break;
			case 3:
				viewer.setRenderMode('smooth');
				JSC3D.console.logInfo('Set to smooth mode.');
				break;
			case 4:
				viewer.setRenderMode('texturesmooth');
				var scene = viewer.getScene();
				if(scene) {
					var objects = scene.getChildren();
					for(var i=0; i<objects.length; i++)
						objects[i].isEnvironmentCast = true;
				}
				JSC3D.console.logInfo('Set to environment-mapping mode.');
				break;
			default:
				viewer.setRenderMode('flat');
				break;
			}
			viewer.update();
		}
    </script>

			<hr>
			<footer class="margin-tb-3">
				<div class="row">
					<div class="col-lg-12">
						<p>Copyright &copy; 3DeeP 2016. <a href="termsofuse.php">Terms of Use</a></p>
					</div>
				</div>
			</footer>
		</div>
		<!-- /.container-fluid -->

		<!-- JQuery scripts -->
	    <script src="assets/js/jquery-1.11.2.min.js"></script>

		<!-- Bootstrap Core scripts -->
		<script src="assets/js/bootstrap.min.js"></script>
  </body>
</html>

